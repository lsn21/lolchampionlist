//
//  PreferencesViewController.swift
//  LolChampionList
//
//  Created by Siarhei on 22.03.17.
//  Copyright © 2017 Siarhei. All rights reserved.
//

import UIKit

class PreferencesViewController: UIViewController {

    var delegate : ViewController = ViewController()
    
    @IBOutlet weak var showIcons: UISwitch!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        showIcons.setOn(delegate.showIcons, animated: true)
    }
    
    // при закрытии окна настроек передаем данные в родительское окно
    @IBAction func doneTapped(_ sender: Any) {
        
        delegate.showIcons = showIcons.isOn
        delegate.tableView.reloadData()
        dismiss(animated: true, completion: nil)

    }
    
}
