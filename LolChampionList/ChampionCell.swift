//
//  ChampionCell.swift
//  LolChampionList
//
//  Created by Siarhei on 22.03.17.
//  Copyright © 2017 Siarhei. All rights reserved.
//

import UIKit

class ChampionCell: UITableViewCell {

    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var freeLab: UILabel!
}
