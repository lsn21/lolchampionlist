
//
//  ViewController.swift
//  LolChampionList
//
//  Created by Siarhei on 20.03.17.
//  Copyright © 2017 Siarhei. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterFree: UISegmentedControl!
    @IBOutlet weak var rightButton: UIBarButtonItem!
    
    var champions = try! Realm().objects(Champion.self).sorted(byKeyPath: "name", ascending: true)
    var showIcons = false
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // устанавливаем ключ и регион
        let lolApi = LolApiWrapper(withKey: "RGAPI-4137644A-06AE-46EB-8381-B8CFBFE6F4F9", andRegion: "ru");
        
        // подключаем базу данных
        let realm = try! Realm()
        
        // удаляем все записи из базы
        try! realm.write {
            realm.deleteAll()
        }
        
        // получаем данные с API "lol-static-data-v1.2" (id, ключ, имя, заголовок)
        let allChampionData = lolApi.lol_static_data(url: "/champion").get().value(forKeyPath: "data")! as! NSDictionary
        
        let championsInfo = lolApi.champion().get()["champions"] as! Array<NSDictionary>
        
        for key in allChampionData.allKeys {
            
            let defaultChampion : Champion = Champion()
            
            let championData = allChampionData[key] as! NSDictionary
            
            defaultChampion.id = championData["id"] as! Int
            defaultChampion.key = championData["key"] as! String
            defaultChampion.name = championData["name"] as! String
            defaultChampion.title = championData["title"] as! String
            
            // получаем данные с API "champion-v1.2" (кто бесплатный)
            for record in (championsInfo) {
                
                if record["id"] as! Int == defaultChampion.id {
                    
                    defaultChampion.free = record["freeToPlay"] as! Bool
                }
            }
            try! realm.write() {
             
                realm.add(defaultChampion)
            }
        }
        
        // настраиваем внешний вид
        searchBar.backgroundColor = UIColor(red: 11.0/255.0, green: 87.0/255.0, blue: 42.0/255.0, alpha: 1.0);
        searchBar.barTintColor = UIColor(red: 0, green: 104.0/255.0, blue: 55.0/255.0, alpha: 1.0)
        
        let preferencesImageView = UIImageView()
        preferencesImageView.contentMode = UIViewContentMode.scaleAspectFit
        
        preferencesImageView.image = UIImage(named: "system_preferences")?.withRenderingMode(.alwaysOriginal)
        rightButton.image = preferencesImageView.image
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ChampionIconsCell") as! ChampionIconsCell
        
        // имя
        cell.nameLabel?.text = champions[indexPath.row].name
        
        // если бесплатный добавляем в отдельное поле слово "Free"
        if champions[indexPath.row].free {
            
            cell.freeLabel?.text = "Free"
        }
        // если нет
        else {
            
            cell.freeLabel?.text = ""
        }
        
        // находим путь до иконки по ключу (key)
        let imageURLString = "http://ddragon.leagueoflegends.com/cdn/7.5.2/img/champion/"+champions[indexPath.row].key+".png"
        
        // загружаем иконки
        cell.iconImage.downloadedFrom(link: imageURLString)
        
        // если в настройках установлено "Показывать иконки"
        if showIcons {
            
            cell.iconImage.isHidden = false
            cell.nameLabelLeading.constant = 54
        }
        // если нет
        else {
            
            cell.iconImage.isHidden = true
            cell.nameLabelLeading.constant = 10
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return champions.count
    }
    
    // поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        var predicate : NSPredicate = NSPredicate()
        let realm = try! Realm()

        // проверяем переключатель "все / бесплатные"
        switch filterFree.selectedSegmentIndex
        {
        // все
        case 0:
            predicate = NSPredicate(format: "name BEGINSWITH [c]%@", searchText)
          
        // бесплатные
        case 1:
            predicate = NSPredicate(format: "free = %i AND name BEGINSWITH [c]%@", 1, searchText)
            
        default:
            break
        }
        // обновляем данные
        champions = realm.objects(Champion.self).sorted(byKeyPath: "name", ascending: true).filter(predicate)
        
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        self.searchBar.showsCancelButton = true
    }
    
    // нажата кнопк "Отмена" в поике
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        
        let realm = try! Realm()
        
        // проверяем переключатель "все / бесплатные"
        switch filterFree.selectedSegmentIndex
        {
        // все
        case 0:
            champions = realm.objects(Champion.self).sorted(byKeyPath: "name", ascending: true)
            
        // бесплатные
        case 1:
            let predicate = NSPredicate(format: "free = %i", 1)
            champions = realm.objects(Champion.self).sorted(byKeyPath: "name", ascending: true).filter(predicate)
            
        default:
            break
        }
        tableView.reloadData()
    }
    
    // сработал переключатель "все / бесплатные"
    @IBAction func filterFreeTapped(_ sender: Any) {
        
        var predicate : NSPredicate = NSPredicate()
        let realm = try! Realm()
        
        switch filterFree.selectedSegmentIndex
        {
        // все
        case 0:
            predicate = NSPredicate(format: "name BEGINSWITH [c]%@", self.searchBar.text!)
            
        // бесплатные
        case 1:
            predicate = NSPredicate(format: "free = %i AND name BEGINSWITH [c]%@", 1, self.searchBar.text!)
            
        default:
            break
        }
        champions = realm.objects(Champion.self).sorted(byKeyPath: "name", ascending: true).filter(predicate)
        
        tableView.reloadData()
    }
    
    // передает параметры в PreferencesViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let preferencesViewController = segue.destination as! PreferencesViewController
        
        preferencesViewController.delegate = self
    }
}

// расширение UIImageView, используется для загрузки иконок
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
