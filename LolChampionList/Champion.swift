//
//  Champion.swift
//  LeagueOfLegendsChampionList
//
//  Created by Siarhei on 20.03.17.
//  Copyright © 2017 Siarhei. All rights reserved.
//

import UIKit
import RealmSwift

class Champion: Object {
    
    dynamic var title : String = ""
    dynamic var id = 0
    dynamic var key = ""
    dynamic var name = ""
    dynamic var free = false
//    dynamic var image = NSData()
}
