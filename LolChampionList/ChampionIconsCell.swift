//
//  ChampionIconsCell.swift
//  LolChampionList
//
//  Created by Siarhei on 22.03.17.
//  Copyright © 2017 Siarhei. All rights reserved.
//

import UIKit

class ChampionIconsCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var freeLabel: UILabel!
    
    @IBOutlet weak var nameLabelLeading: NSLayoutConstraint!
}
